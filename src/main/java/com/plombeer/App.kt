package com.plombeer

import org.jfree.chart.ChartFactory
import org.jfree.chart.ChartPanel
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.plot.XYPlot
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer
import org.jfree.data.xy.XYDataset
import org.jfree.data.xy.XYSeriesCollection
import org.jfree.ui.ApplicationFrame
import java.awt.Color

class App(title: String) : ApplicationFrame(title) {

    private fun createSampleDataset(): XYDataset {
        val series1 = Utils.forSin("Main", 0.01f)

        val dataset = XYSeriesCollection()
        dataset.addSeries(Utils.cubicSpline(Utils.forSin("Main", 0.5f)))
        dataset.addSeries(series1)
        return dataset
    }

    init {
        val dataset = createSampleDataset()
        val chart = ChartFactory.createXYLineChart(
                title,
                "X",
                "Y",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                false,
                false
        )
        val plot = chart.plot as XYPlot
        val renderer = XYLineAndShapeRenderer()
        renderer.setSeriesLinesVisible(0, true)
        renderer.setSeriesShapesVisible(0, false)
        renderer.setSeriesLinesVisible(1, true)
        renderer.setSeriesShapesVisible(1, false)
        plot.renderer = renderer
        plot.getRenderer(0).setSeriesPaint(0, Color.BLUE)
        val chartPanel = ChartPanel(chart)
        chartPanel.preferredSize = java.awt.Dimension(800, 500)
        contentPane = chartPanel
    }
}