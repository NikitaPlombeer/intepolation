package com.plombeer;

import org.jfree.data.xy.XYSeries;

public class Function {

    public interface IFunction {
        double exec(double x);
    }

    private double start;
    private double end;
    private double step;
    private String title;
    private IFunction function;

    public XYSeries countSeries() {
        XYSeries series1 = new XYSeries(title);

        for (double x = start; x <= end; x+= step) {
            series1.add(x, function.exec(x));
        }
        return series1;
    }

    public double[] getPoints() {
        XYSeries series = countSeries();
        double xn[] = new double[series.getItemCount()];
        for (int i = 0; i < xn.length; i++) {
            xn[i] = series.getX(i).doubleValue();
        }
        return xn;
    }

    public double[] getValue() {
        XYSeries series = countSeries();
        double xn[] = new double[series.getItemCount()];
        for (int i = 0; i < xn.length; i++) {
            xn[i] = series.getY(i).doubleValue();
        }
        return xn;
    }


    public static final class FunctionBuilder {
        private double start;
        private double end;
        private double step;
        private String title;
        private IFunction function;

        private FunctionBuilder() {
        }

        public static FunctionBuilder aFunction() {
            return new FunctionBuilder();
        }

        public FunctionBuilder start(double start) {
            this.start = start;
            return this;
        }

        public FunctionBuilder end(double end) {
            this.end = end;
            return this;
        }

        public FunctionBuilder step(double step) {
            this.step = step;
            return this;
        }

        public FunctionBuilder title(String title) {
            this.title = title;
            return this;
        }

        public FunctionBuilder function(IFunction function) {
            this.function = function;
            return this;
        }

        public Function build() {
            Function function = new Function();
            function.start = this.start;
            function.title = this.title;
            function.step = this.step;
            function.end = this.end;
            function.function = this.function;
            return function;
        }
    }
}
