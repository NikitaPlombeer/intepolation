package com.plombeer

import org.jfree.data.xy.XYSeries

interface IFunction {
    fun exec(x: Double): Double
}

data class KFunction(val start: Double = -10.0,
                     val end: Double = 10.0,
                     val step: Double = 0.01,
                     val title: String = "Main",
                     val function: IFunction) {

    fun countSeries(): XYSeries {
        val series1 = XYSeries(title)

        var x = start
        while (x <= end) {
            series1.add(x, function.exec(x))
            x += step
        }
        return series1
    }

    fun getPoints(): DoubleArray {
        val series = countSeries()
        val xn = DoubleArray(series.itemCount)
        for (i in xn.indices) {
            xn[i] = series.getX(i).toDouble()
        }
        return xn
    }

    fun getValue(): DoubleArray {
        val series = countSeries()
        val xn = DoubleArray(series.itemCount)
        for (i in xn.indices) {
            xn[i] = series.getY(i).toDouble()
        }
        return xn
    }

}