package com.plombeer;

import org.jfree.data.xy.XYSeries;

import java.util.ArrayList;
import java.util.List;

public class Utils {


    public static double func(double x) {
        return x * Math.sin(x);
    }

    public static XYSeries forSin(String title, float step) {
        XYSeries series1 = new XYSeries(title);
        double start = -40;
        double end = 40;

        for (double x = start; x <= end; x+= step) {
            series1.add(x, func(x));
        }
        return series1;
    }


    public static XYSeries cubicSpline(XYSeries series) {
        int n = series.getItemCount();
        final double[] xn = Utils.getPoints(series);
        final double[] a = Utils.getValue(series);

        double[] h = new double[n - 1];
        double[] alpha =  new double[n - 1];

        // l, u, z are used in the method for solving the linear system
        double[]  l =  new double[n + 1];
        double[]  u =  new double[n];
        double[]  z =  new double[n + 1];

        // b, c, d will be the coefficients along with a.
        final double[] b = new double[n];
        final double[] c = new double[n + 1];
        final double[] d = new double[n];

        for (int i = 0; i < n - 1; i++)
            h[i] = xn[i + 1] - xn[i];

        for (int i = 1; i < n - 1; i++)
            alpha[i] = (3. / h[i]) * (a[i + 1] - a[i]) - (3. / h[i - 1]) * (a[i] - a[i - 1]);

        l[0] = 1;
        u[0] = 0;
        z[0] = 0;

        for (int i = 1; i < n - 1; i++) {
            l[i] = 2. * (xn[i + 1] - xn[i - 1]) - h[i - 1] * u[i - 1];
            u[i] = h[i] / l[i];
            z[i] = (alpha[i] - h[i - 1] * z[i - 1]) / l[i];
        }

        l[n] = 1.;
        z[n] = 0.;
        c[n] = 0.;

        for (int i = n - 2; i > -1; i--) {
            c[i] = z[i] - u[i] * c[i + 1];
            b[i] = (a[i + 1] - a[i]) / h[i] - h[i] * (c[i + 1] + 2. * c[i]) / 3.;
            d[i] = (c[i + 1] - c[i]) / (3. * h[i]);
        }

        List<Function.IFunction> res = new ArrayList<>();

        for (int i = 0; i < n; i++) {
            final int fI = i;
            res.add(new Function.IFunction() {
                @Override
                public double exec(double x) {
                    return d[fI] * Math.pow(x - xn[fI], 3) + c[fI] * Math.pow(x - xn[fI], 2) + b[fI] * (x - xn[fI]) + a[fI];
                }
            });

            System.out.println(String.format("%f * (x - %f) ^ 3 + %f * (x - %f) ^ 2 + %f * (x - %f) + %f",
                    d[i], xn[i], c[i], xn[i], b[i], xn[i], a[i]));
        }

        XYSeries series2 = new XYSeries("Spline");

        double max = -1;
        for (int i = 0; i < res.size(); i++) {
            if(i != res.size() - 1) {
                for (double xx = xn[i]; xx <= xn[i + 1]; xx += 0.01f) {
                    double y = res.get(i).exec(xx);
                    series2.add(xx, y);
                }

                double xCenter = (xn[i] + xn[i + 1]) / 2.;
                double yMainCenter = func(xCenter);
                double yInterpolCenter = res.get(i).exec(xCenter);

                double abs = Math.abs(yMainCenter - yInterpolCenter);
                if(abs > max)
                    max = abs;

            } else {
                double y = res.get(i).exec(xn[i]);
                series2.add(xn[i], y);
            }
        }

        System.out.println("Max difference: " + max);
        return series2;
    }


    public static double[] getPoints(XYSeries series) {
        double xn[] = new double[series.getItemCount()];
        for (int i = 0; i < xn.length; i++) {
            xn[i] = series.getX(i).doubleValue();
        }
        return xn;
    }

    public static double[] getValue(XYSeries series) {
        double xn[] = new double[series.getItemCount()];
        for (int i = 0; i < xn.length; i++) {
            xn[i] = series.getY(i).doubleValue();
        }
        return xn;
    }
}
