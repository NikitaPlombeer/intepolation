package com.plombeer;

import org.jfree.ui.RefineryUtilities;

public class XYLineAndShapeRendererDemo {


    public static void main(final String[] args) {

        App demo = new App("Interpolation");
        demo.pack();
        RefineryUtilities.centerFrameOnScreen(demo);
        demo.setVisible(true);
    }

}